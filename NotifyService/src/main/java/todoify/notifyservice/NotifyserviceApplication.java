package todoify.notifyservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class NotifyserviceApplication {

  public static void main(String[] args) {
    SpringApplication.run(NotifyserviceApplication.class, args);
  }
}
