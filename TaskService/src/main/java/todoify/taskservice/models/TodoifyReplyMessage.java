package todoify.taskservice.models;
import com.linecorp.bot.model.message.Message;

public class TodoifyReplyMessage {
    String replyToken;
    Message message;


    public TodoifyReplyMessage(String replyToken, Message message) {
        this.replyToken = replyToken;
        this.message = message;
    }

    public String getReplyToken() {
        return replyToken;
    }

    public void setReplyToken(String replyToken) {
        this.replyToken = replyToken;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

}
