package todoify.taskservice.controller;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.*;

import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import todoify.taskservice.models.TodoifyReplyMessage;
import todoify.taskservice.service.*;
import todoify.taskservice.clients.BotServiceClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;


@RestController
@EnableFeignClients
public class TaskController {

    @Autowired
    private BotServiceClient botServiceClient;
    @Autowired
    private AddService addService;
    @Autowired
    private RemoveService removeService;
    @Autowired
    private UpdateService updateService;
    @Autowired
    private ViewService viewService;



    @GetMapping("/handler")
    @ResponseStatus(value = HttpStatus.OK)
    public void handleQuery(@RequestParam("query") String query, @RequestParam("argument") String argument,
                            @RequestParam("replyToken") String replyToken,@RequestParam("userId") String userId) throws ParseException {
        Message message;
        switch (query) {
            case "/add":
                message = addService.handle(argument,userId);
                break;

            case "/remove":
                System.out.println("arguments:  " + argument + "userId:  " + userId);
                message = removeService.handle(argument,userId);

                break;

            case "/update":
                message = updateService.handle(argument,userId);
                break;

            case "/view":
                message = viewService.handle(argument,userId);
                break;

            case "/help":
        message =
            new TextMessage(
                "HELP\n\n==========\n\n"
                    + "/add = To add a task\n\n"
                    + "/remove = To remove a task\n\n"
                    + "/update = To update an existing task\n\n"
                    + "/view = view all your to do's\n\n"
                    + "/addTemplate = gives add template\n\n"
                    + "/removeTemplate = gives remove template\n\n"
                    + "/updateTemplate = gives update template");
                break;

            case "/information":
         message =
             new TextMessage(
                     "Hello world!\n\n"
                     +"Lorem ipsum");
                break;

            case("updateTemplate"):
        message =
            new TextMessage (
                    "/update\n" + "Title:\n" +"Date:dd-mm-yyyy\n" + "Time:hh:mm\n" + "Description:");
                break;

            case("/addTemplate"):
        message =
            new TextMessage(
                "/add\n" + "Title:\n" + "Date:dd-mm-yyyy\n" + "Time:hh:mm\n" + "Description:");
                break;

            case("/removeTemplate"):
        message =
             new TextMessage(
                 "/remove\n" + "Title:");
                break;

            default:
                message = new TextMessage("An Error occured, Sorry I cannot process your query...");
                break;

        }
        this.sendMessage(replyToken, message);
    }

    public void sendMessage(String replyToken, Message message) {
        TodoifyReplyMessage replyMessage = new TodoifyReplyMessage(replyToken, message);
        botServiceClient.send(replyMessage);
    }


}
