package todoify.taskservice.service;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Service;
import todoify.taskservice.entity.TaskModel;
import todoify.taskservice.repository.TaskRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class AddService {
    private final TaskRepository taskRepository;
    public AddService(TaskRepository taskRepository) { this.taskRepository = taskRepository; }

    public Message handle(String argument,String userId) throws ParseException {
        if(argument.equals("none")){
            return new TextMessage(
                    "We cant process your form! please use '/addTemplate' for a valid task form!"
            );
        }else{
            String[] argumentSplitted = argument.split("\n");
            String temp = "";
            for (int i= 0; i < argumentSplitted.length; i++){
                argumentSplitted[i] = argumentSplitted[i].split(":",2)[1];
                temp += argumentSplitted[i]+"\n";
            }

            DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            System.out.println(argumentSplitted[1]);
            Date date = format.parse(argumentSplitted[1]);

            TaskModel task = new TaskModel(userId,argumentSplitted[0], date, argumentSplitted[2]);
            taskRepository.save(task);
            return new TextMessage("Your task been successfully added!");
        }
    }

}
