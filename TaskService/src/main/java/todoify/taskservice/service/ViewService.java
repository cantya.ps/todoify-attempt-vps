package todoify.taskservice.service;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Service;
import todoify.taskservice.entity.TaskModel;
import todoify.taskservice.repository.TaskRepository;

import java.util.List;
import java.util.ArrayList;

@Service
public class ViewService {
    private final TaskRepository taskRepository;

    public ViewService(TaskRepository taskRepository) { this.taskRepository = taskRepository; }

    public Message handle(String argument, String userId){

        List<TaskModel> listData = new ArrayList<>();
        for(TaskModel taskModel : taskRepository.findAll()) {
            if(taskModel.getUserId().equals(userId)){
                listData.add(taskModel);
            }
        }

        String output = "";
        for(TaskModel data : listData) {
            System.out.println("Found data by User Id: "+ data.toString());
            output += "\n " + data.getId() + " - " +data.getTitle();
        }

        System.out.println("Argument : "+ argument+ " ID : "+userId);
        return new TextMessage("Data \n" + output );

    }
}
