package todoify.taskservice.service;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import org.springframework.stereotype.Service;
import todoify.taskservice.entity.TaskModel;
import todoify.taskservice.repository.TaskRepository;
import java.util.Optional;

@Service
public class UpdateService {
    private final TaskRepository taskRepository;

    public UpdateService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Message handle(String argument,String userId){
        if(argument.equals("none")){
            return new TextMessage(
                    "We cant process your form! please use '/removeTemplate' for a valid remove form!"
            );
        }else{
            String[] splittedArgument = argument.split("\n");
            for (int i= 0; i < splittedArgument.length; i++){
                splittedArgument[i] = splittedArgument[i].split(":",2)[1];
                System.out.println("Splitted Argument : " + splittedArgument[i]);
            }

            TaskModel entityFound = new TaskModel();
            boolean hasFound = false;
            for(TaskModel taskModel : taskRepository.findAll()) {
                if(taskModel.getId() == Integer.parseInt(splittedArgument[0])){
                    entityFound = taskModel;
                    hasFound = true;
                }
            }

            System.out.println("Found : "+ entityFound.toString());

            entityFound.setTitle(splittedArgument[1]);
            entityFound.setDescription(splittedArgument[2]);
            taskRepository.save(entityFound);

            if(hasFound) return new TextMessage("Your task been successfully updated!");
            else return new TextMessage("Tidak ada data yang Anda masukkan dalam database");
        }
    }
}
