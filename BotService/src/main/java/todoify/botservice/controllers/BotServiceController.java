package todoify.botservice.controllers;


import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import todoify.botservice.clients.TaskClient;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import todoify.botservice.exception.QueryErrorHandler;
import todoify.botservice.handlers.QueryHandler;
import todoify.botservice.handlers.TaskHandler;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import todoify.botservice.models.TodoifyReplyMessage;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.PostConstruct;
import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

@RestController
@EnableFeignClients
@LineMessageHandler
public class BotServiceController {

    @Autowired
    private TaskClient taskClient;

    @Autowired
    private LineMessagingClient lineMessagingClient;
    
    private QueryHandler abstractQueryHandler;

  @EventMapping
  public void receiveQuery(MessageEvent<TextMessageContent> messageEvent) {
    String replyToken = messageEvent.getReplyToken();
    String userId = messageEvent.getSource().getUserId();
    String inputMessage = messageEvent.getMessage().getText();
    String[] messageParts = inputMessage.split("\n", 2);
    String query = messageParts[0];
    String argument = "none";
    if(messageParts.length > 1){
        argument = messageParts[1];
    }
    try {
      this.abstractQueryHandler.handleQuery(query, argument, replyToken,userId);
    } catch (QueryErrorHandler queryException) {
      String errorMessage = queryException.getMessage();
      TextMessage replyMessageContent = new TextMessage(errorMessage);
      TodoifyReplyMessage replyMessage = new TodoifyReplyMessage(replyToken, replyMessageContent);
      sendMessage(replyMessage);
    }
  }

    @PostConstruct
    private void constructQueryHandler() {
        this.abstractQueryHandler = this.constructTaskHandler();
    }

    private QueryHandler constructTaskHandler() {
        ArrayList<String> acceptedQuery = new ArrayList<>();
        acceptedQuery.add("/add");
        acceptedQuery.add("/remove");
        acceptedQuery.add("/update");
        acceptedQuery.add("/view");
        acceptedQuery.add("/help");
        acceptedQuery.add("/addTemplate");
        acceptedQuery.add("/removeTemplate");
        acceptedQuery.add("/updateTemplate");
        acceptedQuery.add("/information");
        return new TaskHandler(acceptedQuery, taskClient);
    }


    @PostMapping("/send")
    public void sendMessage(@RequestBody TodoifyReplyMessage replyMessage) {
        String replyToken = replyMessage.getReplyToken();
        Message message = replyMessage.getMessage();
        ReplyMessage lineReplyMessage = new ReplyMessage(replyToken, message);
        lineMessagingClient.replyMessage(lineReplyMessage);
    }



}
